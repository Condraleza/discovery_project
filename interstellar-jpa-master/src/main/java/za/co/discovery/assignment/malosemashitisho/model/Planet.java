package za.co.discovery.assignment.malosemashitisho.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Planet {
    @Id
    private String nodeName;

    @NotNull
    @Column(nullable = false)
    private String name;
}
