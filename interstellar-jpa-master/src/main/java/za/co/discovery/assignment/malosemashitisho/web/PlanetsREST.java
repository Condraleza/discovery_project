package za.co.discovery.assignment.malosemashitisho.web;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import za.co.discovery.assignment.malosemashitisho.model.Planet;
import za.co.discovery.assignment.malosemashitisho.service.PlanetsService;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * See <a>baeldung.com/building-a-restful-web-service-with-spring-and-java-based-configuration</a>
 */

@RestController
@RequestMapping("/planets")
public class PlanetsREST {
    @Autowired
    private PlanetsService planetsService;

    @GetMapping
    private List<Planet> listAll() {

        return planetsService.findAllAsList();
    }

    @GetMapping(value = "/search/{searchText}")
    private List<Planet> listPartial(@PathVariable("searchText") String searchText) {
        if (searchText.length() < 3)
            throw new IllegalArgumentException("Please enter at least 3 characters to search for the planet");

        return planetsService.findAllAsList().
                stream().
                filter(planet -> planet.getName().toLowerCase().contains(searchText.toLowerCase()))
                .collect(Collectors.toList());
    }


    @PutMapping
    private void updatePlanet(@RequestBody Planet resource) {
        Optional<Planet> planetWrapper = planetsService.findAllAsList()
                .stream()
                .filter(planet -> Objects.equals(planet.getNodeName(), resource.getNodeName()))
                .findFirst();
        if (!planetWrapper.isPresent())
            throw new IllegalArgumentException(String.format("The planet with node name %s was not found in DB", resource.getNodeName()));

        Planet planet = planetWrapper.get();
        planet.setName(resource.getName());
        planetsService.save(planet);
    }

    @PostMapping
    private void addPlanet(@RequestBody Planet resource) {
        Optional<Planet> planetWrapper = planetsService.findAllAsList()
                .stream()
                .filter(planet -> Objects.equals(planet.getNodeName(), resource.getNodeName()))
                .findFirst();
        if (planetWrapper.isPresent())
            throw new IllegalArgumentException(String.format("The planet with node name %s was found in DB", resource.getNodeName()));

        planetsService.save(resource);
    }

    @DeleteMapping(value = "/node/{nodeName}")
    private void deletePlanet(@PathVariable("nodeName") String nodeName) {
        Optional<Planet> planetWrapper = planetsService.findAllAsList()
                .stream()
                .filter(planet -> Objects.equals(planet.getNodeName(), nodeName))
                .findFirst();
        if (!planetWrapper.isPresent())
            throw new IllegalArgumentException(String.format("The planet with node name %s was not found in DB", nodeName));

        planetsService.delete(planetWrapper.get());
    }
}
