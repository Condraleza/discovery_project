package za.co.discovery.assignment.malosemashitisho.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import za.co.discovery.assignment.malosemashitisho.model.Planet;
import za.co.discovery.assignment.malosemashitisho.model.Route;
import za.co.discovery.assignment.malosemashitisho.service.DataSupplier;
import za.co.discovery.assignment.malosemashitisho.service.PlanetsService;
import za.co.discovery.assignment.malosemashitisho.service.RoutesService;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController
@RequestMapping("/routes")
public class RoutesREST {

    @Autowired
    private RoutesService routesService;

    @Autowired
    private PlanetsService planetsService;

    @Autowired
    private DataSupplier interstellarDTO;

    @GetMapping
    private List<Route> findAll() {
        return routesService.findAllAsList();
    }
    @DeleteMapping(value = "/{routeId}")
    private void deleteRoute(@PathVariable("routeId") Integer routeId) {
        Optional<Route> routeWrapper = routesService.findAllAsList().stream()
                .filter(route -> Objects.equals(routeId, route.getRouteId()))
                .findFirst();
        if (!routeWrapper.isPresent())
            throw new IllegalArgumentException(String.format("No route found with id: %d", routeId));

        routesService.delete(routeWrapper.get());
    }

    @PutMapping
    private void updateRoute(@RequestBody Route resource) {
        Optional<Route> routeWrapper = routesService.findAllAsList().stream()
                .filter(route -> Objects.equals(resource.getRouteId(), route.getRouteId()))
                .findFirst();
        if (!routeWrapper.isPresent())
            throw new IllegalArgumentException(String.format("No route found with id: %d", resource.getRouteId()));

        List<Planet> planets = planetsService.findAllAsList();
        validatePlanet(planets, resource.getPlanetOrigin(),"Origin");
        validatePlanet(planets, resource.getPlanetDestination(),"Destination");

        Optional<Route> invalidRouteWrapper = routesService.findAllAsList().stream()
                .filter(route ->
                        !Objects.equals(resource.getRouteId(), route.getRouteId()) &&
                        Objects.equals(resource.getPlanetOrigin().getNodeName(), route.getPlanetOrigin().getNodeName()) &&
                        Objects.equals(resource.getPlanetDestination().getNodeName(), route.getPlanetDestination().getNodeName()))
                .findFirst();
        if (invalidRouteWrapper.isPresent())
            throw new IllegalArgumentException("Route found with same origin and destination found in the DB, but different ids");

        Route route = routeWrapper.get();
        route.setDelay(resource.getDelay());
        route.setDistance(resource.getDistance());

        routesService.save(route);
    }

    @PostMapping
    private void addRoute(@RequestBody Route resource) {
        Optional<Route> routeWrapper = routesService.findAllAsList().stream()
                .filter(route -> Objects.equals(resource.getRouteId(), route.getRouteId()))
                .findFirst();
        if (routeWrapper.isPresent())
            throw new IllegalArgumentException(String.format("Route found with id: %d is already in the DB", resource.getRouteId()));

        List<Planet> planets = planetsService.findAllAsList();
        validatePlanet(planets, resource.getPlanetOrigin(),"Origin");
        validatePlanet(planets, resource.getPlanetDestination(),"Destination");

        routesService.save(resource);
    }

    private void validatePlanet(List<Planet> planets, Planet resourcePlanet, String type) {
        boolean result = planets.stream()
                .anyMatch(planet -> Objects.equals(planet.getNodeName(), resourcePlanet.getNodeName()) && Objects.equals(planet.getName(), resourcePlanet.getName()));

        if (!result)
            throw new IllegalArgumentException(String.format("%s Planet %s was not found in database",type,resourcePlanet));
    }
}
