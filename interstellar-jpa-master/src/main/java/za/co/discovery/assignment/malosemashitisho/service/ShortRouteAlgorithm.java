package za.co.discovery.assignment.malosemashitisho.service;

import za.co.discovery.assignment.malosemashitisho.model.Planet;
import za.co.discovery.assignment.malosemashitisho.model.Route;

import java.util.*;

/**
 * Copied from
 * <a>https://www.vogella.com/tutorials/JavaAlgorithmsDijkstra/article.html</a>
 */
public class ShortRouteAlgorithm {

    private final List<Planet> nodes;
    private final List<Route> edges;
    private Set<Planet> settledNodes;
    private Set<Planet> unSettledNodes;
    private Map<Planet, Planet> predecessors;
    private Map<Planet, Double> distance;
    private LinkedList<Planet> path = new LinkedList<>();

    public ShortRouteAlgorithm(List<Planet> nodes, List<Route> edges) {
        // create a copy of the array so that we can operate on this array
        this.nodes = nodes;
        this.edges = edges;
    }

    public void execute(Planet source) {
        settledNodes = new HashSet<>();
        unSettledNodes = new HashSet<>();
        distance = new HashMap<>();
        predecessors = new HashMap<Planet, Planet>();
        distance.put(source, 0D);
        unSettledNodes.add(source);
        while (unSettledNodes.size() > 0) {
            Planet node = getMinimum(unSettledNodes);
            settledNodes.add(node);
            unSettledNodes.remove(node);
            findMinimalDistances(node);
        }
    }

    private void findMinimalDistances(Planet node) {
        List<Planet> adjacentNodes = getNeighbors(node);
        for (Planet target : adjacentNodes) {
            if (getShortestDistance(target) > getShortestDistance(node)
                    + getDistance(node, target)) {
                distance.put(target, getShortestDistance(node)
                        + getDistance(node, target));
                predecessors.put(target, node);
                unSettledNodes.add(target);
            }
        }

    }

    private Double getDistance(Planet node, Planet target) {
        for (Route edge : edges) {
            if (edge.getPlanetOrigin().equals(node)
                    && edge.getPlanetDestination().equals(target)) {
                return edge.totalDistance();
            }
        }
        throw new RuntimeException("Should not happen");
    }

    private List<Planet> getNeighbors(Planet node) {
        List<Planet> neighbors = new ArrayList<>();
        for (Route edge : edges) {
            if (edge.getPlanetOrigin().equals(node)
                    && !isSettled(edge.getPlanetDestination())) {
                neighbors.add(edge.getPlanetDestination());
            }
        }
        return neighbors;
    }

    private Planet getMinimum(Set<Planet> vertexes) {
        Planet minimum = null;
        for (Planet vertex : vertexes) {
            if (minimum == null) {
                minimum = vertex;
            } else {
                if (getShortestDistance(vertex) < getShortestDistance(minimum)) {
                    minimum = vertex;
                }
            }
        }
        return minimum;
    }

    private boolean isSettled(Planet vertex) {
        return settledNodes.contains(vertex);
    }

    private Double getShortestDistance(Planet destination) {
        Double d = distance.get(destination);
        if (d == null) {
            return Double.MAX_VALUE;
        } else {
            return d;
        }
    }

    /*
     * This method returns the path from the source to the selected target and
     * NULL if no path exists
     */
    public LinkedList<Planet> getPath(Planet target) {

        Planet step = target;
        // check if a path exists
        if (predecessors.get(step) == null) {
            return null;
        }
        path.add(step);
        while (predecessors.get(step) != null) {
            step = predecessors.get(step);
            path.add(step);
        }
        // Put it into the correct order
        Collections.reverse(path);
        return path;
    }

    public LinkedList<Planet> getPath() {
        return path;
    }

    public void setPath(LinkedList<Planet> path) {
        this.path = path;
    }
}
