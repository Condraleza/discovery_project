package za.co.discovery.assignment.malosemashitisho;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import za.co.discovery.assignment.malosemashitisho.model.Inputs;
import za.co.discovery.assignment.malosemashitisho.model.Planet;
import za.co.discovery.assignment.malosemashitisho.model.Route;
import za.co.discovery.assignment.malosemashitisho.service.DataSupplier;
import za.co.discovery.assignment.malosemashitisho.service.PlanetsService;
import za.co.discovery.assignment.malosemashitisho.service.RoutesService;
import za.co.discovery.assignment.malosemashitisho.service.ShortRouteAlgorithm;

import java.io.File;
import java.nio.file.Paths;
import java.util.List;

@Controller
public class NavigationController {

	@Autowired
	private DataSupplier interstellarDTO;
	@Autowired
	private PlanetsService planetsService;

	@Autowired
	private RoutesService routesService;

	@GetMapping("/routefinder")
	public String routefinderForm(Model model) throws Exception {

		List<Planet> planets = planetsService.findAllAsList();
		model.addAttribute("routefinder", new Inputs()); // data user has selected
		model.addAttribute("planetList", planets); // data to display for user to select
		return "routefinder";


	}

	@PostMapping("/routefinder")
	public String routefinderSubmit(@ModelAttribute Inputs inputs, Model model) {

		int sourceIndex = getIndexOfString(inputs.getSource());
		int destinationIndex = getIndexOfString(inputs.getDestination());
		List<Planet> shortesRoute = getShortesRoute(sourceIndex,destinationIndex);

		getIndexOfString(inputs.getSource());
		if(shortesRoute != null && shortesRoute.size() > 0){
			model.addAttribute("theroute", shortesRoute);
		} else {
			model.addAttribute("theroute", "no route was found with selected source and destination, please make sure you select correctly");
		}

		return "result";
	}


	private List<Planet> getShortesRoute(int source, int destination){
		try {

			List<Planet> planets = planetsService.findAllAsList();
			List<Route> routes = routesService.findAllAsList();
			System.out.println("Called from web source is " + source + " destination is " + destination);
			ShortRouteAlgorithm algorithm = new ShortRouteAlgorithm(planets, routes);
			algorithm.execute(planets.get(source));
			List<Planet> path = algorithm.getPath(planets.get(destination));

			return  path;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	//getting index of nodeID.
	private int getIndexOfString(String input){
		System.out.println("input passed is " + input);
		int finalValue = 0;
		char[] ch  = input.toLowerCase().toCharArray();
		for(char c : ch){
			int temp = (int)c;
			System.out.println("the caracter value is " + temp);
			 int temp_integer = 96; //for lower case
			if(temp<=122 & temp>=97){
				System.out.print(temp-temp_integer);
				finalValue = temp - temp_integer;
			}

		}
		return finalValue -1;
	}
}
