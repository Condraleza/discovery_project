package za.co.discovery.assignment.malosemashitisho.service;

import org.springframework.data.repository.CrudRepository;
import za.co.discovery.assignment.malosemashitisho.model.Route;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public interface RoutesService extends CrudRepository<Route, Integer> {
    default List<Route> findAllAsList() {
        return StreamSupport.stream(
                this.findAll().spliterator(), false).collect(Collectors.toList());
    }
}
