package za.co.discovery.assignment.malosemashitisho.service;

import za.co.discovery.assignment.malosemashitisho.model.Planet;
import za.co.discovery.assignment.malosemashitisho.model.Route;

import java.util.List;

public interface DataSupplier {
    List<Planet> loadPlanets(String... args) throws Exception;
    List<Route> loadRoutes(String... args) throws Exception;
}
