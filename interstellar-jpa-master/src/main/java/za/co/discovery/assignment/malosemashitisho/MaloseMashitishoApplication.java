package za.co.discovery.assignment.malosemashitisho;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.ui.Model;
import za.co.discovery.assignment.malosemashitisho.model.Planet;
import za.co.discovery.assignment.malosemashitisho.model.Route;
import za.co.discovery.assignment.malosemashitisho.service.DataSupplier;
import za.co.discovery.assignment.malosemashitisho.service.PlanetsService;
import za.co.discovery.assignment.malosemashitisho.service.RoutesService;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@SpringBootApplication
@Log
@ComponentScan
public class MaloseMashitishoApplication implements CommandLineRunner {

    @Autowired
    private PlanetsService planetsService;

    @Autowired
    private RoutesService routesService;

    @Autowired
    private DataSupplier interstellarDTO;

    public static void main(String[] args) {
        SpringApplication.run(MaloseMashitishoApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

      //  Path path = Paths.get(args[0]);

      //  System.out.println("file name "+path.toAbsolutePath());
        List<Planet> planets = interstellarDTO.loadPlanets(args);

        planets.stream().forEach(planet -> {
            planetsService.save(planet);
            log.info("Finished saving: " + planet.toString());
        });

        List<Route> routes = interstellarDTO.loadRoutes(args);

        routes.stream().forEach(route-> {
            routesService.save(route);
            log.info("Finished saving: "+route.toString());
        });
    }

}
