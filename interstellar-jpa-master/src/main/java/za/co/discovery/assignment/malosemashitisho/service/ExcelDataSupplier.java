package za.co.discovery.assignment.malosemashitisho.service;

import lombok.extern.java.Log;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.ApplicationScope;
import za.co.discovery.assignment.malosemashitisho.model.Planet;
import za.co.discovery.assignment.malosemashitisho.model.Route;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@Service
@ApplicationScope
@Log
public class ExcelDataSupplier implements DataSupplier {

    @Override
    public ArrayList<Planet> loadPlanets(String... args) throws Exception {
        validateApplicationArguments(args);

        Path dataFile = Paths.get(args[0]);

        doesFileExist(dataFile);

        try (InputStream input = Files.newInputStream(dataFile)) {
            Workbook workbook = new XSSFWorkbook(input);

            Sheet planetNames = workbook.getSheet("Planet Names");

            ArrayList<Planet> planetList = new ArrayList<>();
            Iterator<Row> rows = planetNames.iterator();

            if (rows.hasNext())
                rows.next();//skip heading...

            while (rows.hasNext()) {
                Row row = rows.next();
                String nodeName = row.getCell(0).getStringCellValue();
                String name = row.getCell(1).getStringCellValue();
                planetList.add(new Planet(nodeName, name));
            }

            return planetList;
        }
    }

    @Override
    public ArrayList<Route> loadRoutes(String... args) throws Exception {
        validateApplicationArguments(args);

        Path dataFile = Paths.get(args[0]);

        doesFileExist(dataFile);

        ArrayList<Route> routes = readRouteDataFromSheet(dataFile, "Routes",args);
        ArrayList<Route> routeTraffic = readRouteDataFromSheet(dataFile, "Traffic",args);

        for (Route route:routes) {
            routeTraffic.stream()
                    .filter(trafficRoute->trafficRoute.getRouteId().equals(route.getRouteId()))
                    .forEach(trafficRoute->route.setDelay(trafficRoute.getDistance()));
        }

        return routes;
    }

    private ArrayList<Route> readRouteDataFromSheet(Path dataFile,String sheetName, String[] args) throws Exception {
        List<Planet> planetList = this.loadPlanets(args);

        try (InputStream input = Files.newInputStream(dataFile)) {
            Workbook workbook = new XSSFWorkbook(input);

            Sheet sheet = workbook.getSheet(sheetName);

            Iterator<Row> rows = sheet.rowIterator();

            ArrayList<Route> routeList = new ArrayList<>();

            if (rows.hasNext())
                rows.next();//skip heading...

            while (rows.hasNext()) {
                Row row = rows.next();
                Double routeId = row.getCell(0).getNumericCellValue();
                String origin = row.getCell(1).getStringCellValue();
                String destination = row.getCell(2).getStringCellValue();
                Double distance = row.getCell(3).getNumericCellValue();

                try {
                    Route route = new Route(
                            routeId.intValue(),
                            getPlanet(origin, planetList),
                            getPlanet(destination, planetList),
                            distance,
                            0D);
                    routeList.add(route);
                }
                catch(RuntimeException re) {
                    log.warning(re.getMessage());
                }
            }

            return routeList;
        }
    }

    private Planet getPlanet(String nodeName, List<Planet> planetList) {
        Optional<Planet> result = planetList.stream()
                .filter(planet -> Objects.equals(nodeName, planet.getNodeName()))
                .findFirst();

        if (!result.isPresent())
            throw new RuntimeException("Couldn't find "+nodeName);
        return result.get();
    }

    private void doesFileExist(Path dataFile) {
        if (Files.notExists(dataFile))
            throw new IllegalArgumentException(String.format("The file specified %s does not exist", dataFile));
    }

    private void validateApplicationArguments(String[] args) {
        if (args.length != 1)
            throw new IllegalArgumentException("Please specify the name of the file containing planet and distance data when running this application");
    }
}
