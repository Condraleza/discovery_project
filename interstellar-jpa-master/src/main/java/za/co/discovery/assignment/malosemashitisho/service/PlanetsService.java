package za.co.discovery.assignment.malosemashitisho.service;

import org.springframework.data.repository.CrudRepository;
import za.co.discovery.assignment.malosemashitisho.model.Planet;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


public interface PlanetsService extends CrudRepository<Planet, String> {
    default List<Planet> findAllAsList() {
        return StreamSupport.stream(
                this.findAll().spliterator(), false)
                .collect(Collectors.toList());
    }
}
