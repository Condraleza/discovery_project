package za.co.discovery.assignment.malosemashitisho.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Route {
    @Id
    private Integer routeId;

    @OneToOne
    @NotNull
    private Planet planetOrigin;

    @OneToOne
    @NotNull
    private Planet planetDestination;

    @NotNull
    @Column(nullable = false)
    private Double distance;

    @NotNull
    @Column(nullable = false)
    private Double delay;

    public Double totalDistance() {
        return distance+delay;
    }
}
